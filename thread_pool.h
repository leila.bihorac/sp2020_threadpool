#ifndef thread_pool_
#define thread_pool_
#include <thread>
#include <functional>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <future>
#include <vector>
#include <atomic>
#include <type_traits>
#include <iostream>
#include <stdexcept>

using function_t = std::function<void()>;

class task_queue
{
public:
  bool blocking_pop(function_t &fun)
  {
    std::unique_lock<std::mutex> queue_lock{mtx_};
    while (tasks_.empty())
    {
      if (stopped_)
        return false;
      cv_.wait(queue_lock);
    }
    fun = std::move(tasks_.front());
    tasks_.pop();
    return true;
  }

  template <typename F>
  void push(F &&f)
  {
    {
      std::unique_lock<std::mutex> queue_lock{mtx_};
      tasks_.emplace(std::forward<F>(f));
    }
    cv_.notify_one();
  }

  void stop()
  {
    {
      std::unique_lock<std::mutex> queue_lock{mtx_};
      stopped_ = true;
    }
    cv_.notify_all();
  }

  bool isEmpty()
  {
    return tasks_.empty();
  }

private:
  std::queue<function_t> tasks_;
  std::mutex mtx_;
  std::condition_variable cv_;
  bool stopped_ = false;
};

class thread_pool
{
public:
  thread_pool(size_t th_num = std::thread::hardware_concurrency()) : thread_number_{th_num}, queue_number_{0}
  {
    for (int i = 0; i < thread_number_; ++i)
    {
      queues_.emplace_back(std::make_unique<task_queue>());
    }

    for (int i = 0; i < thread_number_; ++i)
    {
      threads_.emplace_back(std::thread{[this, i]() { run(i); }});
    }
  }

  ~thread_pool()
  {
    for (int i = 0; i < thread_number_; ++i)
    {
      queues_[i]->stop();
    }

    for (auto &t : threads_)
      t.join();
  }

  template <typename T>
  void async(T &&fun)
  {
    (queues_[queue_number_])->push(std::forward<T>(fun));
    // tasks_.push(std::forward<T>(fun));
    ++queue_number_;
    if (queue_number_ >= thread_number_)
      queue_number_ = 0;
  }

private:
  void run(int position)
  {
    while (true)
    {
      std::atomic<size_t> i;
      function_t fun;
      
        i = position;
      

      while (true)
      {
        //std::cout << i << " " << thread_number_<< std::endl;
        if (!queues_[i]->isEmpty())
          break;
        i++;
        if (i >= thread_number_)
           i = 0;
        
        if (i == position)
          break;
      }

      if (!(queues_[i])->blocking_pop(fun))
        return;

      fun();
    }
  }

  const size_t thread_number_;
  //std::mutex mtx;
  std::vector<std::thread> threads_;
  //task_queue tasks_;
  std::vector<std::unique_ptr<task_queue>> queues_;
  std::atomic<size_t> queue_number_;
  static std::atomic<size_t> task_number_;
};

#endif /* ifndef thread_pool_ */

